## Topic

# Concurrency - Thread safe code
- Understand live lock, dead lock, starvation
- Thread safe classes, colections: CopyOnWriteArrayList(), ConcurentHashmap()
- Lock:
+ Lock and ReadWriteLock interface, lock(), tryLock() return boolean
- Synchonyze method and block vs Lock: when exception, synch method release optimstic lock itself while Lock does not that why have to use try finally

# Concurrency - Runnable, Callable, Lifecycle, Executors
- Thread pool:
+ Executor, ExceuterService interface
+ Executors util class
+ Callable, Runable
+ Future, Future.get() will be block untill it done
+ shutDown(), shutDownNow(), 
+ Type of pool: SingleThreadExecutor, schedul, fix, cache
# Concurrency - atomic package
- AtomicInteger(), this class used in thread safe because it have atomic function and it have volatile int inside
- Functions: get(), set(), incrementAndGet(), addAndGet(int delta)